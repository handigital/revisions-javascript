# Quelques exercices pour réviser Javascript

Javascript permet de manipuler le DOM d'une page web. Le DOM (Document Object Model) est une représentation du code HTML sous la forme d'un arbre. Chaque balise représente un élément de l'arbre et il peut avoir zéro, un ou plusieurs enfants. Les enfants peuvent eux-mêmes avoir des enfants. Et ainsi de suite.

Il est possible de récupérer des informations à partir du DOM, mais aussi de le modifier pour changer le style d'un élément, d'ajouter des éléments dans la page ou d'en supprimer. Il est également possible de récupérer certains événements (un clic sur un élément par exemple), ce qui va nous permettre de rendre la page interactive


## La sélection d'un élément du DOM

Tout démarre avec `document`. C'est un objet auquel vous avez directement accès dans votre code Javascript. Cet object a des méthodes. Une méthode est une fonction attachée à un objet.

Par le passé, pour sélectionner un élément du DOM, on utilisait les méthodes `.getElementById()`, `.getElementsByClassName()` et `.getElementsByTagName()`.

Par exemple, si on a le code HTML suivant :

```html
<ul>
    <li class="legume">Artichaud</li>
    <li class="fruit">Kiwi</li>
    <li class="fruit" id="leMeilleurFruit">Melon</li>
    <li class="fruit">Poire</li>
</ul>
```

On peut récupérer les éléments de la manière suivante en Javascript :

```js
// Sélectionne uniquement l'élément Melon
let mon_fruit_prefere = document.getElementById("leMeilleurFruit");
// Sélectionne trois éléments : Kiwi, Melon et Poire
let tableau_de_fruits = document.getElementsByClassName("fruit");
// Sélectionne les 4 éléments : Artichaud, Kiwi, Melon et Poire
let tous_les_aliments = document.getElementsByTagName("li");
```

Ces méthodes avaient quelques limitations. Pour s'en affranchir, on a mis en place deux méthodes beaucoup plus puissantes : `.querySelector()` et `.querySelectorAll()`. Elles utilisent les sélecteurs CSS pour identifier les éléments dans la page.

`.querySelector()` récupère le premier élément dans la page qui correspond au sélecteur. S'il y en a plusieurs, seul le premier est renvoyé. `.querySelectorAll()` récupère tous les éléments qui correspondent au sélecteur.

Quelques exemples avec le même code HTML que tout à l'heure :

```js
// Sélectionne uniquement l'élément Melon
let mon_fruit_prefere = document.querySelector("#leMeilleurFruit");
// Sélectionne trois éléments : Kiwi, Melon et Poire
let tableau_de_fruits = document.querySelectorAll(".fruit");
// Sélectionne uniquement l'élément Kiwi
let premier_fruit = document.querySelector(".fruit");
// Sélectionne les 4 éléments : Artichaud, Kiwi, Melon et Poire
let tous_les_aliments = document.querySelectorAll("li");
```

Bien sûr, on peut imaginer des sélecteurs CSS plus compliqué. Comme `document.querySelectorAll("header p")` pour récupérer tous les paragraphes qui se trouvent dans le _header_ de la page.


Lisez la documentation pour en savoir plus sur les méthodes vues :

 - [`.getElementById()`](https://developer.mozilla.org/fr/docs/Web/API/Document/getElementById) ;
 - [`.getElementsByClassName()`](https://developer.mozilla.org/fr/docs/Web/API/Document/getElementsByClassName) ;
 - [`.getElementsByTagName()`](https://developer.mozilla.org/fr/docs/Web/API/Document/getElementsByTagName) ;
 - [`.querySelector()`](https://developer.mozilla.org/fr/docs/Web/API/Document/querySelector) ;
 - [`.querySelectorAll()`](https://developer.mozilla.org/fr/docs/Web/API/Document/querySelectorAll).

Quelque soit la méthode utilisée, la sélection d'un élément nous permet ensuite de le manipuler. On va pouvoir récupérer les informations ou les modifier. Par exemple, on peut récupérer et afficher le contenu de la balise `<li>` décrivant mon fruit préféré grâce à la propriété `.innerHTML` :

```js
console.log(document.querySelector("#leMeilleurFruit"));
```

Il est également possible de naviguer vers les autres éléments à partir d'un élément que vous avez déjà sélectionner grâce aux propriétés `.children` et `.parentElement`.

```js
// Sélectionne le parent d'un élément de la liste, c'est-à-dire l'élément <ul>
document.querySelector("#leMeilleurFruit").parentElement;
// Sélectionne tous les éléments de la liste
document.querySelector("ul").children;
```

À vous de jouer à présent en modifiant la page `selection.html`. Ajoutez une balise script et réalisez les tâches suivantes :

 1. Récupérez le titre de la page de 3 manières différentes et affichez-le dans la console ;
 2. Sélectionnez le quatrième élément de la liste de course et affichez-le ;
 3. Sélectionnez tous les fruits rouges et affichez-les ;
 4. Sélectionnez tous les éléménts de la liste de course de deux manières différentes et affichez-le.


## La création d'un élément et son ajout au DOM

Pour créer un élément, on peut utiliser la méthode [`.createElement()`](https://developer.mozilla.org/fr/docs/Web/API/Document/createElement). Cependant, créer un élément ne suffit pas à le faire apparaître sur la page. Il faut qu'il soit ajouté au DOM, grâce à la méthode [`.appendChild()`](https://developer.mozilla.org/fr/docs/Web/API/Node/appendChild) qu'on appliquera sur le parent devant accueillir le nouvel élément. C'est seulement à ce moment là qu'il apparaîtra sur la page web.

Ajoutez une balise `<div>` ne fera pas grand chose si elle n'a pas de contenu. On peut donc utiliser la propriété [`.innerHTML`](https://developer.mozilla.org/fr/docs/Web/API/Element/innerHTML) pour mettre du contenu dans notre élément ([`.textContent`](https://developer.mozilla.org/fr/docs/Web/API/Node/textContent) marche également).

> Cette partie est plus courte que la précédente et il n'y a même pas d'exemple ! N'hésitez pas à lire la documentation des méthodes. Des exemples sont présents.

À vous de jouer à présent en modifiant la page `modification.html`. Ajoutez une balise script et réalisez les tâches suivantes :

 1. Ajoutez un titre secondaire `<h2>` dans le _header_ ;
 2. Ajoutez deux fruits à la liste de course.


## La modification du DOM

Certaines propriétés des éléments du DOM sont modifiables. D'autres sont en lecture seule.

Pour modifier un attribut HTML, on pourra utiliser la méthode [`.setAttribute()`](https://developer.mozilla.org/fr/docs/Web/API/Element/setAttribute).

Pour modifier le style de l'élément, on peut accéder à chacune des propriétés CSS : `element.style.color` ou `element.style.backgorundColor` (les tirets n'étant pas autorisé dans les noms de propriété, ils sont enlevés et remplacés par une majuscule sur la lettre suivante).

Pour modifier les classes d'un élément, le mieux est de passer par la propriété [`.classList`](https://developer.mozilla.org/fr/docs/Web/API/Element/classList). Les fonctions d'ajout `.add()` et de retrait `.remove()` sont très pratiques. Si on utilisait [`.className`](https://developer.mozilla.org/fr/docs/Web/API/Element/className) à la place, il faudrait faire des recherches dans des chaînes de caractères pour être sûr de ne pas supprimer les autres classes lors de l'ajout ou de la supression d'une classe sur un élément.

Ensore une petite chose bien pratique : le suppression d'un élément se fait à l'aide de la méthode [`.removeChild()`](https://developer.mozilla.org/fr/docs/Web/API/Node/removeChild).

À vous de jouer à présent en modifiant la page `modification.html`. Ajoutez une balise script et réalisez les tâches suivantes :

 1. Ajoutez un fruit rouge à la liste ;
 2. Ajoutez la classe `rouge` au fruit précédemment ajouté ;
 3. Supprimez le premier élément de la liste de course ;
 4. Modifiez la couleur du Kiwi pour qu'il appraisse en vert ;
 5. Changez la marge basse du titre pour qu'elle fasse au moins `100px` ;
 6. Changez l'URL du lien.


## Les événements

Il existe de nombreux événements sur une page web. Ils sont parfois déclenché par l'utilisateur (par exemple, quand il clique quelque part sur la page) et parfois par la machine (par exemple, il y a un événement qui se déclenche à la fin de la lecture d'un son ou d'une vidéo).

[`.addEventListener()`](https://developer.mozilla.org/fr/docs/Web/API/EventTarget/addEventListener) est la méthode permettant de dire qu'on souhaite être attentif et déclencher une action quand tel type d'événement se produit sur tel élément du DOM.

La méthode prend au moins deux arguments : le nom de l'événement (par exemple, `click` ou `keydown`) et une fonction qui détaille l'action à mener une fois que l'événement se déclenche.

La fonction qu'on donne à `.addEventListener()` est encore appelée fonction de _callback_. Elle prend un argumet qui correspond à l'événement qui a été déclenché. On peut ainsi récupérer les informations sur l'événement en cours (voir la [documentation](https://developer.mozilla.org/en-US/docs/Web/API/Event)).

À vous de jouer à présent en modifiant la page `evenement.html`. Ajoutez une balise script et réalisez les tâches suivantes :

 1. Ajoutez un écouteur d'événement de type `click` sur le bouton qui ajoute un élément `<li>` à la liste des plats. Vous pouvez mettre toujours le même plat. Des patates, encore des patates, toujours des patates !


## D'autres questions ?

Si vous avez d'autres questions sur Javascript, c'est le moment ou jamais pour les poser ! N'hésitez pas à m'appeler pour demander de l'aide ou à m'écrire sur Discord.
